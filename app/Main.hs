{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
module Main where

import Concur.Core
import Concur.Replica
import qualified Data.Text as T

import Prelude hiding (div, span)
import Network.Wai.Handler.Replica (Context)
import Data.Text (Text)
import Data.Time.LocalTime (zonedTimeToUTC, ZonedTime)
import Data.SafeCopy
import Data.Acid
import Control.Concurrent (killThread, forkIO)
import Data.List (sortOn, find)
import Control.Monad.Reader (MonadReader(ask))
import Control.Monad.IO.Class (MonadIO(liftIO))
import Form
import Control.Monad.State.Class (modify)
import Control.Monad
import Data.Function ((&))
import qualified Network.Wai.Handler.Replica as R
import Network.WebSockets (defaultConnectionOptions)
import qualified Network.Wai.Handler.Warp as W
import Data.FileEmbed
import Data.Functor ((<&>))
import GHC.Generics (Generic)
import Data.Generics.Labels ()
import Control.Lens hiding (Context)

newtype Location = Location { getLocationName :: Text }
    deriving (Eq, Ord, Show, Generic)
data Route = Route
    { from :: Location
    , to :: Location
    , departure :: ZonedTime
    , arrival :: ZonedTime
    , cost :: Int }
    deriving (Show, Generic)
data User = User
    { name :: Text
    , routes :: [Route] }
    deriving (Show, Generic)
data Database = Database
    { users :: [User]
    , routes :: [Route]
    , locations :: [Location] }
    deriving (Show, Generic)

$(deriveSafeCopy 0 'base ''Location)
$(deriveSafeCopy 0 'base ''Route)
$(deriveSafeCopy 0 'base ''User)
$(deriveSafeCopy 0 'base ''Database)

getRoutesForUser :: Text -> Query Database [Route]
getRoutesForUser name' = do
    Database usrs _ _ <- ask
    case find (\User{..} -> name == name') usrs of
        Nothing -> return []
        Just User{..} -> return routes

getLocations :: Query Database [Location]
getLocations = locations <$> ask

addLocation :: Location -> Update Database ()
addLocation loc = do
    modify (\db -> db { locations = loc : locations db })

getMatchingRoutes :: ZonedTime -> Location -> Query Database [Route]
getMatchingRoutes after from = ask
    <&> view #routes
    <&> filter \r ->
            r ^. #from == from
            && zonedTimeToUTC (r ^. #departure) >= zonedTimeToUTC after

$(makeAcidic ''Database ['getRoutesForUser, 'getLocations, 'addLocation, 'getMatchingRoutes])

displayUser :: User -> Widget HTML a
displayUser User{..} = div []
    [ span [] [text name]]

displayRoute :: Route -> Widget HTML a
displayRoute Route{..} = div []
    [ span [] [text (getLocationName from <> " -> " <> getLocationName to)] ]

data State
    = NoLogin
    | LoggedIn
        { currentUser :: Text }
    deriving (Show)

enterName :: Widget HTML Text
enterName = go ""
    where
    go currentInput = do
        inp <- div []
            [ label [] [text "Name"]
            , Left . targetValue . target
                <$> input [type_ "text", onInput, value currentInput, autofocus True]
            , br []
            , Right <$> input [type_ "submit", onClick] ]
        case inp of
            Left inp -> go inp
            Right _ -> return currentInput

data ExistingLocationFormAction
    = SelectAutocompleted Location
    | TypeSearch Text
    | Enter
    | OtherKey Text
    deriving (Show)

-- searchable :: AcidState Database -> Form (Either Text a)
-- searchable db getAll = widgetToForm (Left "") \search -> do
--     search <- either pure (pure . getLocationName) search
--     locs <- unsafeBlockingIO (query db GetLocations)
--     let found = locs
--             & filter ((search `T.isPrefixOf`) . getLocationName)
--             & sortOn getLocationName
--     let choices = concatMap
--             (\loc ->
--                 [ const (SelectAutocompleted loc) <$> div
--                     [onClick, className "location-autocomplete-option"]
--                     [text (getLocationName loc)] ])
--             found
--     choices <- if null choices
--         then return [text "No location found, press ENTER to save current search as new location"]
--         else return choices
--     inp <- div [className "location-search"]
--         [ input
--             [ type_ "text"
--             , value search
--             , TypeSearch . targetValue . target <$> onInput
--             , (\k -> if k == "Enter" then Enter else OtherKey k) . kbdKey <$> onKeyUp
--             , autofocus True ]
--         , div [] choices ]
--     case inp of
--         TypeSearch inp -> return (unsubmitted (Left inp))
--         SelectAutocompleted loc -> return (submitted (Right loc))
--         Enter
--             | null found -> do
--                 liftIO (update db (AddLocation (Location search)))
--                 return (submitted (Right (Location search)))
--             | otherwise -> return (submitted (Right (head found)))
--         OtherKey _ -> return (unsubmitted (Left search))

locationSelectForm :: AcidState Database -> Form (Either Text Location)
locationSelectForm db = eitherForm "" \search -> do
    locs <- unsafeBlockingIO (query db GetLocations)
    let found = locs
            & filter ((search `T.isPrefixOf`) . getLocationName)
            & sortOn getLocationName
    let choices = concatMap
            (\loc ->
                [ const (SelectAutocompleted loc) <$> div
                    [onClick, className "location-autocomplete-option"]
                    [text (getLocationName loc)] ])
            found
    choices <- if null choices
        then return [text "No location found, press ENTER to save current search as new location"]
        else return choices
    inp <- div [className "location-search"]
        [ input
            [ type_ "text"
            , value search
            , TypeSearch . targetValue . target <$> onInput
            , (\k -> if k == "Enter" then Enter else OtherKey k) . kbdKey <$> onKeyUp
            , autofocus True ]
        , div [] choices ]
    case inp of
        TypeSearch inp -> return (Update inp)
        SelectAutocompleted loc -> return (Submit loc)
        Enter
            | null found -> do
                liftIO (update db (AddLocation (Location search)))
                return (Submit (Location search))
            | otherwise -> return (Submit (head found))
        OtherKey _ -> return (Update search)

locationForm :: AcidState Database -> Form (Maybe Location)
locationForm db = locationSelectForm db
    & onSubmit (\case
        Left _ -> return Nothing
        Right loc -> return (Just loc)
        )

locationWidget :: AcidState Database -> Maybe Location -> Form (Maybe Location)
locationWidget db loc = widgetToForm loc pure \loc' -> do
    let locName = maybe "" getLocationName loc'
    _ <- div [onClick, className "location-autocomplete-option"] [text locName]
    Update <$> formToWidget (locationForm db)

data RouteSelectState = Searching Text

-- routeSelectForm :: AcidState Database -> ZonedTime -> Location -> Form (Maybe Route)
-- routeSelectForm db after from = widgetToForm (Searching "") \rt -> do
--     rts <- query db (GetMatchingRoutes after from)



app :: AcidState Database -> State -> Context -> Widget HTML ()
app db NoLogin ctx = do
    name <- enterName
    app db (LoggedIn name) ctx
app db LoggedIn{..} _ = do
    routes <- liftIO (query db (GetRoutesForUser currentUser))
    loc <- div []
        [ div [] (fmap displayRoute routes)
        , formToWidget (locationWidget db (Just (Location "Ludbreg"))) ]
    -- loc <- formToWidget (locationForm db)
    void (div [onClick] [text ("Location: " <> T.pack (show loc))])

customStyle :: Text
customStyle = $(makeRelativeToProject "style.css" >>= embedStringFile)

runApp :: (R.Context -> Widget HTML a) -> IO ()
runApp widget = W.run 8080 $ R.app
    (defaultIndex "Trip planner"
        [ VNode "style" mempty Nothing [VText customStyle] ])
    defaultConnectionOptions
    Prelude.id
    (Concur.Core.step <$> widget)
    stepWidget

main :: IO ()
main = do
    db <- openLocalStateFrom "db/" (Database [] [] [Location "Ludbreg", Location "Zagreb", Location "Zadar"])
    tId <- forkIO $ do
        runApp (app db NoLogin)
    _ <- getChar
    closeAcidState db
    killThread tId
