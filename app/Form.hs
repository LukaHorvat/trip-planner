{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
module Form where

import Prelude hiding (div)
import Concur.Core.Types (unsafeBlockingIO, Widget)
import Replica.VDOM.Types (HTML)
import Concur.Replica
import Data.Void (Void)
import Data.Text (Text)
import Control.Monad (ap)
import Control.Applicative (Alternative(empty))
import Control.Monad.IO.Class (MonadIO(liftIO))
import Data.Maybe (catMaybes)
import Data.Functor (($>))

-- data Event update submit = Update update | Submit submit

-- instance Functor (Event update) where
--     fmap _ (Update u) = Update u
--     fmap f (Submit s) = Submit (f s)

-- mapUpdate :: (update -> update') -> Event update submit -> Event update' submit
-- mapUpdate f (Update u) = Update (f u)
-- mapUpdate _ (Submit s) = Submit s

-- -- | Forms are widgets with an internal state that gets updated on input.
-- --   A form can be submitted at any time which means there has to be a way to project the final
-- --   result out of it's current state.
-- --   Forms can be composed applicatively. This places their UI elements next to each other and
-- --   combines their results. When any of the constituent forms is submitted, the whole form is.
-- data Form a where
--     Form
--         :: state -- ^ The initial state
--         -> (state -> [Widget HTML (Event update a)]) -- ^ The main rendering function.
--         -> (state -> update -> state) -- ^ What to do when there's an update from the UI
--         -> (state -> Widget HTML a) -- ^ How to get the final result if the form gets submitted
--         -> Form a
-- instance Functor Form where
--     fmap f (Form s w u p) = Form s (fmap (fmap (fmap f)) . w) u (fmap f . p)
-- instance Applicative Form where
--     pure a = Form () (\() -> [return (Submit a)]) (\() () -> ()) (\() -> pure a)
--     Form s1 w1 u1 p1 <*> Form s2 w2 u2 p2 =
--         Form (s1, s2) w u p
--         where
--         w (s1', s2') = (f1 s2' <$> w1 s1') <> (f2 s1' <$> w2 s2')
--         u (s1', s2') (Left e1) = (u1 s1' e1, s2')
--         u (s1', s2') (Right e2) = (s1', u2 s2' e2)
--         p (s1', s2') = p1 s1' <*> p2 s2'
--         f1 s2 w = w >>= \case
--             Update u -> return (Update (Left u))
--             Submit r -> Submit . r <$> p2 s2
--         f2 s1 w = w >>= \case
--             Update u -> return (Update (Right u))
--             Submit r -> Submit . ($ r) <$> p1 s1

-- formToWidget :: Form a -> Widget HTML a
-- formToWidget (Form s w u _) = go s
--     where
--     go s' = do
--         evt <- div [] (w s')
--         case evt of
--             Update e -> go (u s' e)
--             Submit res -> pure res

-- widgetToForm :: state -> (state -> Widget HTML a) -> (state -> Widget HTML (Event state a)) -> Form a
-- widgetToForm initial fin w = Form initial (\a -> [w a]) (\_ a -> a) fin

-- -- | A form that either submits with a result, or just returns it's last state if it gets externally
-- --  submitted.
-- eitherForm :: state -> (state -> Widget HTML (Event state res)) -> Form (Either state res)
-- eitherForm initial fn = widgetToForm initial (pure . Left) (fmap (fmap Right) . fn)

-- htmlToForm :: Widget HTML Void -> Form ()
-- htmlToForm w = widgetToForm () (const (pure ())) (\() -> absurd <$> w)

-- textForm :: Form Text
-- textForm = widgetToForm "" pure
--     (\t -> Update . targetValue . target <$> input [type_ "text", onInput, value t])

-- onSubmit :: (a -> Widget HTML b) -> Form a -> Form b
-- onSubmit f (Form s w u p) = Form s (fmap tran . w) u (f <=< p)
--     where
--     tran w' = w' >>= \case
--         Update u -> return (Update u)
--         Submit a -> Submit <$> f a

-- submitForm :: Text -> a -> Form (Maybe a)
-- submitForm txt res = widgetToForm Nothing pure
--     (\_ -> Submit . const (Just res) <$> input [type_ "submit", onClick, value txt])

-- locationSelectForm :: AcidState Database -> Form (Either Text Location)
-- locationSelectForm db = eitherForm "" \search -> do
--     locs <- unsafeBlockingIO (query db GetLocations)
--     let found = locs
--             & filter ((search `T.isPrefixOf`) . getLocationName)
--             & sortOn getLocationName
--     let choices = concatMap
--             (\loc ->
--                 [ const (SelectAutocompleted loc) <$> div
--                     [onClick, className "location-autocomplete-option"]
--                     [text (getLocationName loc)] ])
--             found
--     choices <- if null choices
--         then return [text "No location found, press ENTER to save current search as new location"]
--         else return choices
--     inp <- div [className "location-search"]
--         [ input
--             [ type_ "text"
--             , value search
--             , TypeSearch . targetValue . target <$> onInput
--             , (\k -> if k == "Enter" then Enter else OtherKey k) . kbdKey <$> onKeyUp
--             , autofocus True ]
--         , div [] choices ]
--     case inp of
--         TypeSearch inp -> return (Update inp)
--         SelectAutocompleted loc -> return (Submit loc)
--         Enter
--             | null found -> do
--                 liftIO (update db (AddLocation (Location search)))
--                 return (Submit (Location search))
--             | otherwise -> return (Submit (head found))
--         OtherKey _ -> return (Update search)

data ConcatWidget a where
    Widget :: Widget HTML a -> ConcatWidget a
    Two :: ConcatWidget a -> ConcatWidget a -> ConcatWidget a
    Bind :: ConcatWidget a -> (a -> ConcatWidget b) -> ConcatWidget b
instance Monad ConcatWidget where
    return a = Widget (return a)
    (>>=) = Bind
instance Applicative ConcatWidget where
    pure = return
    (<*>) = ap
instance Functor ConcatWidget where
    fmap f a = a >>= (pure . f)

data Form state where
    FromWidget :: state -> (state -> Widget HTML e) -> (state -> e -> state) -> Form state
    Depends :: Form state -> (state -> Form state2) -> Form state2
    IOForm :: Bool -> IO state -> Form state

getState :: Form state -> Widget HTML state
getState (FromWidget s _ _) = pure s
getState (Depends form f) = do
    s <- getState form
    getState (f s)
getState (IOForm blocking i) = if blocking then unsafeBlockingIO i else liftIO i

updateState :: state -> Form state -> Form state
updateState s (FromWidget _ w u) = FromWidget s w u
updateState s (Depends form f) = Depends form (updateState s . f)
updateState _ (IOForm b i) = IOForm b i

data ContForm state where
    ContForm :: [Widget HTML ev] -> (ev -> Widget HTML (Form state)) -> ContForm state

stepForm :: Form state -> Widget HTML (ContForm state)
stepForm (FromWidget i w u) = pure (ContForm [w i] (\e -> pure (FromWidget (u i e) w u)))
stepForm (Depends form1 f) = do
    ContForm w1 u1 <- stepForm form1
    s1 <- getState form1
    let form2 = f s1
    ContForm w2 u2 <- stepForm form2
    return (ContForm ((fmap Left <$> w1) <> (fmap Right <$> w2)) (\e ->
        case e of
            Left e -> do
                form1' <- u1 e
                return (Depends form1' f)
            Right e -> do
                form2' <- u2 e
                s2' <- getState form2'
                return (Depends form1 (updateState s2' . f))))
stepForm (IOForm b i) = pure (ContForm [] (\() -> pure (IOForm b i)))

formToWidget :: Form state -> Widget HTML state
formToWidget f = do
    ContForm ws u <- stepForm f
    e <- div [] ws
    f' <- u e
    formToWidget f'

instance Functor Form where
    fmap f a = a >>= (pure . f)
instance Applicative Form where
    pure = return
    (<*>) = ap
instance Monad Form where
    return a = FromWidget a (const empty) (\a _ -> a)
    (>>=) = Depends
instance MonadIO Form where
    liftIO = IOForm False

widgetToForm :: state -> (state -> Widget HTML e) -> (state -> e -> state) -> Form state
widgetToForm = FromWidget

htmlToForm :: Widget HTML Void -> Form ()
htmlToForm w = widgetToForm () (const w) (\() _  -> ())

loopWidget :: state -> (state -> Widget HTML state) -> Form state
loopWidget initial w = widgetToForm initial w (\_ t -> t)

textForm :: Form Text
textForm = loopWidget ""
    (\t -> input [type_ "text", targetValue . target <$> onInput, value t])

submitForm :: Text -> a -> Form (Maybe a)
submitForm txt res = loopWidget Nothing
    (\_ -> const (Just res) <$> input [type_ "submit", onClick, value txt])

unsafeBlockingIOForm :: IO state -> Form state
unsafeBlockingIOForm = IOForm True

test = do
    choice <- mapM (\t -> loopWidget Nothing (\_ -> div [Just t <$ onClick] [text t])) ["test1", "test2", "test3"]
    htmlToForm $ case catMaybes choice of
        [] -> text "No selection"
        (c : _) -> text c